package it.edoardocostantini.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.edoardocostantini.model.Centro;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.ResponsabileService;
import it.edoardocostantini.validator.CentroValidator;






@Controller
public class CentroController {

	@Autowired
	private CentroValidator validator;
	
	@Autowired
	CentroService centroService;
	
	@Autowired
	ResponsabileService respService;
	
	@GetMapping("/centri")
	public String attivita(Model model) {
		List<Centro> centri = centroService.findAll();
		model.addAttribute("listacentri",centri);
		return "centri";
	}
	
	@GetMapping("/aggiungicentro")
	public String aggiungiCentro(Model model) {
		model.addAttribute("centro",new Centro());
		return "direttore/aggiungicentro";
	}
	
	
	@RequestMapping(value = "/registracentro", method = RequestMethod.POST)
	public String registraCentro(@Valid @ModelAttribute("centro") Centro centro,
			BindingResult bindingResult,Model model) {
		this.validator.validate(centro, bindingResult);

		if (this.centroService.alreadyExists(centro)) {
			model.addAttribute("exists", "il centro già esiste.");
			return "direttore/aggiungicentro";
		} else {
			if (!bindingResult.hasErrors()) {
				this.centroService.save(centro);
				return "redirect:/areadirettore";
			}
		}

		return "direttore/aggiungicentro";
	}
	
	@RequestMapping(value = "/centro/{id}", method = RequestMethod.GET)
    public String getCentro(@PathVariable("id") Long id, Model model) {
		Centro centro = this.centroService.findById(id);
		model.addAttribute("listaattivita",centro.getAttivita());
		model.addAttribute("centro",centro );
    	return "centro";
    }
	
	
	@RequestMapping(value = "/gestiscicentro/{id}", method = RequestMethod.GET)
    public String gestisciCentro(@PathVariable("id") Long id, Model model) {
		Centro centro = this.centroService.findById(id);
		model.addAttribute("centro",centro );
        model.addAttribute("listaresponsabili",respService.findAll());
    	return "/direttore/gestiscicentro";
    }
	
	@RequestMapping(value = "/nomina/{idc}/{idr}", method = RequestMethod.GET)
    public String nomina(@PathVariable("idc") Long idc,@PathVariable("idr") Long idr, Model model) {
		Centro centro = this.centroService.findById(idc);
		Responsabile responsabile = this.respService.findById(idr);
		centro.setResponsabile(responsabile);
		responsabile.setCentro(centro);
		model.addAttribute("centro",centro );
        model.addAttribute("listaresponsabili",respService.findAll());
		return "redirect:/areadirettore";
    }
	
	
}
