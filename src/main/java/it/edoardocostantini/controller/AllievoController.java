package it.edoardocostantini.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.edoardocostantini.model.Allievo;
import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.AllievoService;
import it.edoardocostantini.service.AttivitaService;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.ResponsabileService;
import it.edoardocostantini.validator.AllievoValidator;

@Controller
public class AllievoController {
	
	@Autowired
	CentroService centroService;
	@Autowired
	ResponsabileService respService;
	@Autowired
	private AttivitaService attService;
	@Autowired
	private AllievoService allievoService;
	@Autowired
	private AllievoValidator validator;
	
	
	@GetMapping("/cercaAllievo")
	public String cercaAllievo(Model model) {
		model.addAttribute("allievo",new Allievo());
		return "responsabile/cercaAllievo";
	}
	
	
	@GetMapping("/aggiungiAllievo")
	public String aggiungiAllievo(Model model){
		model.addAttribute("allievo", new Allievo());
		return "responsabile/aggiungiallievo";
	}
	
	@RequestMapping(value = "/registraAllievo", method = RequestMethod.POST)
	public String registraAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, BindingResult bindingResult, Model model) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		this.validator.validate(allievo, bindingResult);

		if(this.allievoService.alreadyExist(allievo)) {
			model.addAttribute("exists", "L'allievo è già iscritto.");
			return "responsabile/aggiungiallievo";
		}
		else 
			if(!bindingResult.hasErrors()) {
				Centro centro  = respService.findByUsername(user.getUsername()).getCentro();
				this.allievoService.save(allievo);
				centro.addAllievo(allievo);
				allievo.setCentro(centro);
				this.allievoService.save(allievo);
				this.centroService.save(centro);
				return "redirect:/arearesponsabile";
			}
		
		return "responsabile/aggiungiallievo";
		
	}
	

	@RequestMapping(value = "/findAllievo", method = RequestMethod.POST)
	public String getAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, Model model,
			BindingResult bindingResult) {
		this.validator.validate(allievo, bindingResult);
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Responsabile responsabile = respService.findByUsername(user.getUsername());
		List<Allievo> listAllievo = this.allievoService.findByEmail(allievo.getEmail()); 
		
		if(listAllievo.size()>0) {
			
		if (this.allievoService.findByEmail(allievo.getEmail()) != null) {
			model.addAttribute("allievo", listAllievo.get(0));
			model.addAttribute("listaattivita", attService.findByCentro(responsabile.getCentro()));
			return "responsabile/mostraAllievo";
		} else {
			if (!bindingResult.hasFieldErrors("email")) {
				return "responsabile/cercaAllievo";
			}
		}
		
		}
		return "responsabile/allievoCerca";
	}
	
	@RequestMapping(value = "/iscrivi/{idall}/{idatt}", method = RequestMethod.GET)
    public String nomina(@PathVariable("idall") Long idall,@PathVariable("idatt") Long idatt, Model model) {
		Allievo allievo = this.allievoService.findById(idall);
		Attivita attivita = this.attService.findById(idatt);
		allievo.addAttivita(attivita);
		attivita.addAllievo(allievo);
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Responsabile responsabile = respService.findByUsername(user.getUsername());
		
		model.addAttribute("listaallievi", responsabile.getCentro().getAllievi() );
        model.addAttribute("listaattivita",responsabile.getCentro().getAttivita());
		return "redirect:/arearesponsabile";
    }
}
