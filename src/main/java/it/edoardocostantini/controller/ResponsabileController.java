package it.edoardocostantini.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.edoardocostantini.model.Centro;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.AttivitaService;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.ResponsabileService;
import it.edoardocostantini.validator.ResponsabileValidator;

@Controller
public class ResponsabileController {
	@Autowired
	ResponsabileValidator validator;
	@Autowired
	AttivitaService attService;
	@Autowired
	CentroService centroService;
	@Autowired
	ResponsabileService respService;
	@GetMapping("/arearesponsabile")
	public String dashboard(Model model, HttpSession session) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails user = (UserDetails) auth.getPrincipal();
		Responsabile responsabile= respService.findByUsername(user.getUsername());
		model.addAttribute("responsabile",responsabile);
		model.addAttribute("centro",responsabile.getCentro());
		model.addAttribute("listaallievi", responsabile.getCentro().getAllievi() );
        model.addAttribute("listaattivita",responsabile.getCentro().getAttivita());
		return "responsabile/arearesponsabile";
		
	}
	
	@GetMapping("/aggiungiresponsabile")
	public String aggiungiResponsabile(Model model) {
		model.addAttribute("responsabile",new Responsabile());
		return "direttore/aggiungiresponsabile";
	}
	
	@RequestMapping(value = "/registraresponsabile", method = RequestMethod.POST)
	public String registraCentro(@Valid @ModelAttribute("responsabile") Responsabile responsabile,
			BindingResult bindingResult ,Model model) {
		this.validator.validate(responsabile, bindingResult);

		if (this.respService.alreadyExists(responsabile)) {
			model.addAttribute("exists", "il centro già esiste.");
			return "direttore/aggiungiresponsabile";
		} else {
			if (!bindingResult.hasErrors()) {
				this.respService.save(responsabile);
				return "redirect:/areadirettore";
			}
		}

		return "direttore/aggiungicentro";
	}
}
