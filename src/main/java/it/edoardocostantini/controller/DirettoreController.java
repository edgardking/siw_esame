package it.edoardocostantini.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.DirettoreService;
import it.edoardocostantini.service.ResponsabileService;

@Controller
public class DirettoreController {
	
	@Autowired
	DirettoreService dirService;
	@Autowired
	ResponsabileService respService;
	@Autowired
	CentroService centroService;
	
	@GetMapping("/areadirettore")
	public String dashboard(Model model) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("direttore", dirService.findByUsername(user.getUsername()));
		model.addAttribute("listaresponsabili", respService.findAll());
		model.addAttribute("listacentri",centroService.findAll());
		return "direttore/areadirettore";
		
	}
	
	
	
	
}
