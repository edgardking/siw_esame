package it.edoardocostantini.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.edoardocostantini.model.Allievo;
import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;
import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.AllievoService;
import it.edoardocostantini.service.AttivitaService;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.DirettoreService;
import it.edoardocostantini.service.ResponsabileService;

@Controller
public class WebController {





	@GetMapping("/index")
	public String index(Model model) {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		List<Attivita> dailyActivities = attService.findByGiorno(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
		//List<Attivita> dailyActivities = attService.findByGiorno("Monday");
		model.addAttribute("listattivita", dailyActivities);
		return "index";
	}


	@Autowired
	AttivitaService attService;
	@Autowired
	AllievoService allService;
	@Autowired
	ResponsabileService respService;
	@Autowired
	DirettoreService dirService;
	@Autowired
	CentroService cenService;


	/**
	@GetMapping("/login")
		public String login(HttpSession session,@RequestParam("email") String email,@RequestParam("password") String password) {
			Responsabile resp=respService.findByEmail(email).get(0);
			if(resp.getPassword().equals(password)) {
				session.setAttribute("id", resp.getId());
				return "arearesponsabile";
			}
			return "login";
		}**/

	
	@GetMapping("/chisiamo")
	public String chiSiamo(Model model) {
		return "chisiamo";
	}
	
	@RequestMapping("/login")
	public String login(Model model){
		Collection<? extends GrantedAuthority> authorities= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities){
			if (grantedAuthority.getAuthority().equals("ROLE_RESPONSABILE")) {
				return "redirect:/arearesponsabile";
			}
			if (grantedAuthority.getAuthority().equals("ROLE_DIRETTORE")) {
				return "redirect:/areadirettore";
			}

		}
		return "login";

	}

	@RequestMapping(value= "/")
	protected String showDashboard(Model model, HttpSession session) {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		List<Attivita> dailyActivities = attService.findByGiorno(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
		//List<Attivita> dailyActivities = attService.findByGiorno("Tuesday");
		model.addAttribute("listattivita", dailyActivities);
		

		Collection<? extends GrantedAuthority> authorities= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities){
			if (grantedAuthority.getAuthority().equals("ROLE_RESPONSABILE")) {
				return "redirect:/arearesponsabile";
			}
			if (grantedAuthority.getAuthority().equals("ROLE_DIRETTORE")) {
				
				return "redirect:/areadirettore";
			}

		}


		return "index";
	}

	@GetMapping("/responsabili")
	public String responsabili(Model model) {
		model.addAttribute("listaresponsabili", respService.findAll());
		return "responsabili";
	}
	/**
	@GetMapping("/attivita")
	public String attivita(Model model) {
		model.addAttribute("listaattivita",attService.findAll());
		return "attivita";
	}**/
	

}
