package it.edoardocostantini.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.edoardocostantini.model.Allievo;
import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;
import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.AllievoService;
import it.edoardocostantini.service.AttivitaService;
import it.edoardocostantini.service.CentroService;
import it.edoardocostantini.service.DirettoreService;
import it.edoardocostantini.service.ResponsabileService;
@RestController
public class DebugController {

	@Autowired
	AttivitaService attService;
	@Autowired
	AllievoService allService;
	@Autowired
	ResponsabileService respService;
	@Autowired
	DirettoreService dirService;
	@Autowired
	CentroService cenService;

	/**@SuppressWarnings("deprecation")
	@GetMapping("/initdatabase")
	public void init(){
		Direttore dir = new Direttore();

		Responsabile res1 = new Responsabile();
		Responsabile res2 = new Responsabile();

		Centro cen1 = new Centro();
		Centro cen2 = new Centro();

		Attivita att1 = new Attivita();
		Attivita att2 = new Attivita();
		Attivita att3 = new Attivita();
		Attivita att4 = new Attivita();
		Attivita att5 = new Attivita();


		Allievo all1 = new Allievo();
		Allievo all2 = new Allievo();


		if(dirService.findByUsername("costantini@acmeformazione.it")==null) {
		dir.setUsername("costantini@acmeformazione.it");
		dir.setNome("Edoardo");
		dir.setCognome("Costantini");
		dir.setTelefono("3663306355");
		dir.setPassword("edoardo");
		dirService.save(dir);
		}
		if(respService.findByUsername("ungaro@acmeformazione.it")==null) {
		res1.setNome("Riccardo");
		res1.setCognome("Ungaro");
		res1.setUsername("ungaro@acmeformazione.it");
		res1.setTelefono("3452029385");
		res1.setPassword("ungaro");
		respService.save(res1);
		}
		if(respService.findByUsername("pierfederici@acmeformazione.it")==null) {
		res2.setNome("Flavio");
		res2.setCognome("Pierfederici");
		res2.setUsername("pierfederici@acmeformazione.it");
		res2.setTelefono("3452321385");
		res2.setPassword("pierfederici");
		respService.save(res2);
		}
		cen1.setNome("CentroUngaro");
		cen1.setEmail("centroungaro@acmeformazione.it");
		cen1.setNumeroTelefono("0650309603");
		cen1.setIndirizzo("Via irolli 91");
		cen1.setCapienza(50);


		cen2.setNome("CentroPierfederici");
		cen2.setEmail("centroPierfederici@acmeformazione.it");
		cen2.setNumeroTelefono("06503023603");
		cen2.setIndirizzo("Via divino amore 291");
		cen2.setCapienza(50);

		cenService.save(cen1);
		cenService.save(cen2);

		att1.setNome("Inglese");
		att1.setGiorno("Monday");
		att1.setOra("16");
		att1.setCentro(cen1);
		attService.save(att1);

		att2.setNome("Java");
		att2.setGiorno("Tuesday");
		att2.setOra("12");
		att2.setCentro(cen1);
		attService.save(att2);

		att3.setNome("PHP");
		att3.setGiorno("Monday");
		att3.setOra("16");
		att3.setCentro(cen1);
		attService.save(att3);

		att4.setNome("Marketing");
		att4.setGiorno("Friday");
		att4.setOra("18");
		att4.setCentro(cen2);
		attService.save(att4);

		att5.setNome("SQL");
		att5.setGiorno("Tuesday");
		att5.setOra("13");
		att5.setCentro(cen2);
		attService.save(att5);

		all1.setNome("Mattia");
		all1.setCognome("Tarquinio");
		all1.setDataNascita(new Date(1996,12,10));
		all1.setEmail("mattia@tarquinio.it");
		all1.setResidenza("Via Stiriola di tribu 32");
		//all1.addAttivita(att5);
		all1.addAttivita(att1);
		allService.save(all1);

		all2.setNome("Andrea");
		all2.setCognome("Ceccarini");
		all2.setDataNascita(new Date(1996,5,18));
		all2.setEmail("andrea@ceccarini.it");
		all2.setResidenza("Via ione tribu 32");
		all2.addAttivita(att3);
		all2.addAttivita(att2);
		allService.save(all2);


		cen1.addAttivita(att1);
		cen1.addAttivita(att2);
		cen2.addAttivita(att3);
		cen2.addAttivita(att4);
		cen2.addAttivita(att5);

	}
	 **/

	@RequestMapping("/inseriscidirettore")
	public void debugDirettore(@RequestParam("username") String username,@RequestParam("password") String password) {

		Direttore d = new Direttore();
		d.setUsername(username);
		d.setPassword(password);
		dirService.save(d);
	}
	
	@RequestMapping("/inserisciresponsabile")
	public void debugResponsabile(@RequestParam("username") String username,
								  @RequestParam("password") String password,
								  @RequestParam("nome") String nome,
								  @RequestParam("cognome") String cognome ,
								  @RequestParam("telefono") String telefono) {

		Responsabile d = new Responsabile();
		d.setUsername(username);
		d.setPassword(password);
		d.setCognome(cognome);
		d.setNome(nome);
		d.setTelefono(telefono);
		respService.save(d);
	}
	
	@RequestMapping("/debugCentro")
	public void debugCentro(@RequestParam("nome") String nome,
							@RequestParam("email") String email,
							@RequestParam("telefono") String telefono,
							@RequestParam("capienza") String capienza,
							@RequestParam("indirizzo") String indirizzo,
							@RequestParam("idr") String idR) {

		Centro c = new Centro();
		c.setNome(nome);
		c.setEmail(email);
		c.setCapienza(Integer.parseInt(capienza));
		c.setIndirizzo(indirizzo);
		c.setNumeroTelefono(telefono);
		cenService.save(c);
		Responsabile r = respService.findById(Long.parseLong(idR));
		c.setResponsabile(r);
		r.setCentro(c);
	}



	@RequestMapping(value = "/debugallievo", method = RequestMethod.POST )
	public void debugInserimento(@RequestParam("nome") String nome,
								 @RequestParam("cognome") String cognome,
								 @RequestParam("residenza") String residenza,
								 @RequestParam("email") String email) {
		
		Allievo a = new Allievo();
		a.setNome(nome);
		a.setCognome(cognome);
		a.setEmail(email);
		a.setResidenza(residenza);
		allService.save(a);

	}
	
	@RequestMapping(value = "/debugallievoattivita", method = RequestMethod.POST )
	public void debugInserimento(@RequestParam("nomeattivita") String nome,
								 @RequestParam("emailallievo") String email) {
		Allievo a = (Allievo) allService.findByEmail(email).get(0);
		Attivita att = attService.findByNome(nome).get(0);
		
		a.addAttivita(att);
		att.addAllievo(a);
		
		allService.save(a);
		attService.save(att);
	}
	
}
