package it.edoardocostantini.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.edoardocostantini.model.*;
import it.edoardocostantini.service.AttivitaService;
import it.edoardocostantini.service.ResponsabileService;
import it.edoardocostantini.validator.AttivitaValidator;


@Controller
public class AttivitaController {
	@Autowired
	private AttivitaService attService;
	@Autowired
	private AttivitaValidator validator;
	@Autowired
	AttivitaService attivitaService;
	@Autowired
	ResponsabileService respService;
	
	@GetMapping("/attivita")
	public String attivita(Model model) {
		model.addAttribute("attivita",new Attivita());
		List<Attivita> listAttivita = attivitaService.findAll();
		model.addAttribute("listattivita",listAttivita);
		return "attivita";
	}
	
	@GetMapping("/aggiungiAttivita")
    public String aggiungiAttivita(Model model) {
        model.addAttribute("attivita", new Attivita());
        return "responsabile/aggiungiattivita";
    }
	
	@RequestMapping(value = "/attivita/{id}", method = RequestMethod.GET)
    public String getCentro(@PathVariable("id") Long id, Model model) {
		Attivita attivita = this.attService.findById(id);
		model.addAttribute("listaallievi",attivita.getAllievi());
		model.addAttribute("centro",attivita.getCentro() );
    	return "mostraattivita";
    }
	
	@RequestMapping(value = "/registraAttivita", method = RequestMethod.POST)
	public String registraNuovaAttivita(@Valid @ModelAttribute("attivita") Attivita attivita, BindingResult bindingResult,Model model ) {
		this.validator.validate(attivita, bindingResult);
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Responsabile responsabile = respService.findByUsername(user.getUsername());
		
		if (this.attivitaService.alreadyExists(attivita)) {
			model.addAttribute("exists", "L'attività già esiste.");
			return "responsabile/aggiungiattivita";
		} else {
			if (!bindingResult.hasErrors()) {
				responsabile.getCentro().addAttivita(attivita);
				attivita.setCentro(responsabile.getCentro());
				this.attivitaService.save(attivita);
				return "redirect:/arearesponsabile";
			}
		}
		return "responsabile/aggiungiattivita";
	}
}
