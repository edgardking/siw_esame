package it.edoardocostantini.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;
import it.edoardocostantini.repository.CentroRepository;

@Transactional
@Service
public class CentroService{
	@Autowired
	CentroRepository repo;
	
	public void save(Centro centro) {
		repo.save(centro);
	}
	
	
	public Centro findByNome(String nome){
		
		return repo.findByNome(nome).get(0);
	}
	
	
	public List<Centro> findAll(){
		return (List<Centro>) repo.findAll();
	}
	
	public Centro findById(Long id) {
		Optional<Centro> centro =  repo.findById(id);
		if(centro.isPresent())
			return centro.get();
		else
			return null;
	}
	
	public Centro findByNumeroTelefono(String telefono) {
		return repo.findByNumeroTelefono(telefono);
	}
	


	public boolean alreadyExists(@Valid Centro centro) {
		if(repo.findByNome(centro.getNome()).size()>0)
			return true;
		else 
			return false;
		
	}
}
