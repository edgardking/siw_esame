package it.edoardocostantini.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.repository.DirettoreRepository;

public class DirettoreUserDetailsService implements UserDetailsService {

	@Autowired
	DirettoreRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Direttore responsabile = repository.findByUsername(username);

	   UserBuilder builder=null;
	    if (responsabile!=null) {
	      builder = org.springframework.security.core.userdetails.User.withUsername(username);
	      builder.password(responsabile.getPassword());
	      builder.roles(responsabile.getRole());
	    } 
	    else {
	      throw new UsernameNotFoundException("User not found.");
	    }

	    return builder.build();
		
	}

}
