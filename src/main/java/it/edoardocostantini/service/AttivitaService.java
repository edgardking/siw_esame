package it.edoardocostantini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;
import it.edoardocostantini.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {
	@Autowired
	AttivitaRepository repo;
	
	public void save(Attivita att) {
		repo.save(att);
	}
	
	public List<Attivita> findByGiorno(String day){
		return repo.findByGiorno(day);
	}
	
	public List<Attivita> findByNome(String nome){
		return repo.findByNome(nome);
	}
	
	public List<Attivita> findByNomeAndGiorno(String nome, String day){
		return repo.findByNomeAndGiorno(nome, day);
	}
	
	public List<Attivita> findAll(){
		return (List<Attivita>) repo.findAll();
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> attivita =  repo.findById(id);
		if(attivita.isPresent())
			return attivita.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Attivita attivita) {
		if(repo.findByNomeAndGiorno(attivita.getNome(),attivita.getGiorno()).size()>0)
			return true;
		else 
			return false;
	}
	
	public List<Attivita> findByCentro(Centro centro){
		return this.repo.findByCentro(centro);	
		}
	
}
