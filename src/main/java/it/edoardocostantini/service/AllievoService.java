package it.edoardocostantini.service;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.edoardocostantini.model.Allievo;
import it.edoardocostantini.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {

	@Autowired
	AllievoRepository repo;
	
	public void save(Allievo all) {
		repo.save(all);
	}
	
	public List<Allievo> findByNome (String nome){
		return repo.findByNome(nome);
	}
	
	public List<Allievo> findByCognome (String cognome){
		return repo.findByCognome(cognome);
	}
	
	public List<Allievo> findByDataNascita (Date dataNascita){
		return repo.findByDataNascita(dataNascita);
	}
	
	public List<Allievo> findByEmail (String email){
		return repo.findByEmail(email);
	}
	
	public List<Allievo> findByResidenza (String residenza){
		return repo.findByResidenza(residenza);
	}
	
	
	
	public List<Allievo> findByNomeAndCognome (String nome, String cognome){
		return repo.findByNomeAndCognome(nome, cognome);
	}
	
	public List<Allievo> findByNomeAndCognomeAndDataNascita (String nome, String cognome, Date dataNascita){
		return repo.findByNomeAndCognomeAndDataNascita(nome, cognome, dataNascita);
	}
	
	public List<Allievo> findByNomeAndCognomeAndEmail (String nome, String cognome, String email){
		return repo.findByNomeAndCognomeAndEmail(nome, cognome, email);
	}
	
	public List<Allievo> findByNomeAndCognomeAndDataNascitaAndEmail (String nome, String cognome, Date dataNascita, String email){
		return repo.findByNomeAndCognomeAndDataNascitaAndEmail(nome, cognome, dataNascita, email);
	}
	
	
	public List<Allievo> findAll(){
		return (List<Allievo>) repo.findAll();
	}
	
	public Allievo findById(Long id) {
		Optional<Allievo> allievo = repo.findById(id);
		if(allievo.isPresent())
			return allievo.get();
		else
			return null;
	}
	
	public boolean alreadyExist(Allievo allievo) {
		if(repo.findByNomeAndCognomeAndDataNascitaAndEmail(allievo.getNome(), allievo.getCognome(), allievo.getDataNascita(), allievo.getEmail()).size() > 0)
			return true;
		else
			return false;
	}
}
