package it.edoardocostantini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {

	@Autowired
	ResponsabileRepository repo;
	
	public void save(Responsabile resp) {
		repo.save(resp);
	}
	
	public List<Responsabile> findByNome (String nome){
		return repo.findByNome(nome);
	}
	
	public List<Responsabile> findByCognome (String cognome){
		return repo.findByCognome(cognome);
	}
	
	public Responsabile findByUsername (String email){
		return repo.findByUsername(email);
	}
	
	public List<Responsabile> findByTelefono (String telefono){
		return repo.findByTelefono(telefono);
	}
	
	public List<Responsabile> findByNomeAndCognome (String nome, String cognome){
		return repo.findByNomeAndCognome(nome, cognome);
	}
	
	public List<Responsabile> findAll(){
		return (List<Responsabile>) repo.findAll();
	}
	
	public Responsabile findById(Long id) {
		Optional<Responsabile> responsabile = repo.findById(id);
		
		if(responsabile.isPresent())
			return responsabile.get();
		else
			return null;
	}
	Responsabile findByPassword(String password) {
		return repo.findByPassword(password);
	}
	public boolean alreadyExists(Responsabile responsabile) {
		if(repo.findByNomeAndCognome(responsabile.getNome(), responsabile.getCognome()).size() > 0)
			return true;
		else
			return false;
	}
}
