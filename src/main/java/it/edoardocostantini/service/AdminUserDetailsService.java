package it.edoardocostantini.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.repository.DirettoreRepository;
import it.edoardocostantini.repository.ResponsabileRepository;

public class AdminUserDetailsService implements UserDetailsService {

	@Autowired
	ResponsabileRepository respRepository;

	@Autowired
	DirettoreRepository direRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Object user = (Responsabile) respRepository.findByUsername(username);


		UserBuilder builder=null;
		if (user!=null) {
			builder = org.springframework.security.core.userdetails.User.withUsername(username);
			builder.password(((Responsabile) user).getPassword());
			builder.roles(((Responsabile) user).getRole());
			return builder.build();

		} else {
			user =(Direttore) direRepository.findByUsername(username);
			
			if (user!=null) {
				builder = org.springframework.security.core.userdetails.User.withUsername(username);
				builder.password(((Direttore) user).getPassword());
				builder.roles(((Direttore) user).getRole());
				return builder.build();
			} 

		}

		throw new UsernameNotFoundException(null);
	}

}
