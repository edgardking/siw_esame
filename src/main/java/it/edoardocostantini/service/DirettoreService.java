package it.edoardocostantini.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.model.Direttore;
import it.edoardocostantini.repository.DirettoreRepository;

@Service
@Transactional
public class DirettoreService {
	@Autowired
	DirettoreRepository dir;
	public Direttore findByUsername(String username) {
		return dir.findByUsername(username);
	}
	public Direttore findByPassword(String password) {
		return dir.findByPassword(password);
	}
	
	public void save(Direttore direttore) {
		dir.save(direttore);
	}
	
	public List<Direttore> findByNome (String nome){
		return dir.findByNome(nome);
	}
	
	public List<Direttore> findByCognome (String cognome){
		return dir.findByCognome(cognome);
	}
	
	public Direttore findById(Long id) {
		Optional<Direttore> direttore = dir.findById(id);
		
		if(direttore.isPresent())
			return direttore.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Direttore direttore) {
		if(dir.findByUsername(direttore.getUsername())!=null)
			return true;
		else
			return false;
	}
	
}
