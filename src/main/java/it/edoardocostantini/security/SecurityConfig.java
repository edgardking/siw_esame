package it.edoardocostantini.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.edoardocostantini.service.AdminUserDetailsService;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Bean
	public UserDetailsService userDetailsService() {
		return new AdminUserDetailsService();
	}
	
	
	

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
	return new BCryptPasswordEncoder();
	}
	
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(bCryptPasswordEncoder());
	}
	
	@Override
    public void configure(WebSecurity web) {
    web .ignoring()
    .antMatchers("/static/**", "/css/**", "/images/**", "/js/**", "/vendor/**"); }
 
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http .csrf().disable()
    	.authorizeRequests()
    	.antMatchers("/login","/","/inseriscidirettore","/attivita","/centri","/responsabili").permitAll() 
    	.antMatchers("/direttore/**").hasRole("DIRETTORE") 
    	.antMatchers("/responsabile/**").hasRole("RESPONSABILE")  
    	.and()
    	.formLogin()
    	.loginPage("/login")
    	//.defaultSuccessUrl("/" )
    	.and()
    	.logout()
    	.logoutSuccessUrl("/").permitAll()
    	.and()
	    .csrf().disable();
	}
}
