package it.edoardocostantini.model;

import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Attivita {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	private String descrizione;
	
	@Column(nullable=false)
	private String giorno;

	@Column(nullable=false)
	private String ora;
	
	
	@ManyToMany
	@JoinColumn(name="attivita")
	private List<Allievo> allievi;

	@ManyToOne
	@JoinColumn(name="centro_id")
	private Centro centro;
	
	public Attivita() {
		
	}
	
	public Attivita( String nome, String giorno, String ora, List<Allievo> allievi, Centro centro) {
		super();
		this.nome = nome;
		this.giorno = giorno;
		this.ora = ora;
		this.allievi = allievi;
		this.centro = centro;
	}
	
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public String getGiorno() {
		return giorno;
	}

	public void setGiorno(String giorno) {
		this.giorno = giorno;
	}

	public String getOra() {
		return ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}
	
	public void aggiungiAllievo(Allievo a) {
		this.allievi.add(a); 
	}

	public void addAllievo(Allievo a) {
			this.allievi.add(a);
	}
	

}
