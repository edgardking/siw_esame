package it.edoardocostantini.model;

import java.text.*;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Allievo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String cognome;
	
	@Column(nullable=false, unique=true)
	private String email;
	
	private String telefono;
	
	@Column(nullable=false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern ="dd/MM/yyyy")
	private Date dataNascita;
	
	
	private String residenza;
	
	@ManyToMany(mappedBy="allievi")
	private List<Attivita> attivita;
	
	@ManyToOne
	private Centro centro;
	
	public Allievo() {
		
	}
	
	
	public Allievo(String nome, String cognome, String email, String telefono, Date dataNascita, String residenza) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.setTelefono(telefono);
		this.residenza = residenza;
		this.dataNascita=dataNascita;
	}
	



	public Centro getCentro() {
		return centro;
	}


	public void setCentro(Centro centro) {
		this.centro = centro;
	}


	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}


	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getDataNascita() {
		return dataNascita;
	}
	
	public void setDataNascita(String dataNascita) {		
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
		try {
            this.dataNascita = formatter.parse(dataNascita);
        } catch (ParseException e) {
            e.printStackTrace();
        }		
	}
	
	public String getResidenza() {
		return residenza;
	}
	
	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}
	
	public void addAttivita(Attivita a ) {
		this.attivita.add(a);
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
}
