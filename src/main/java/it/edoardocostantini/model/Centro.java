package it.edoardocostantini.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private int capienza;
	
	@Column(nullable=false)
	private String indirizzo;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private String numeroTelefono;
	
	@OneToMany
	private List<Allievo> allievi;

	@OneToMany(mappedBy="centro")
	private List<Attivita> attivita;


	@OneToOne
	private Responsabile responsabile;


	public Centro() {

	}

	public Centro(String nome, int capienza, String indirizzo, String email, String telefono, Responsabile responsabile) {
		super();
		this.nome = nome;
		this.capienza = capienza;
		this.indirizzo = indirizzo;
		this.email = email;
		this.numeroTelefono = telefono;
		this.responsabile = responsabile;
	}


	
	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}
	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}
	public Responsabile getResponsabile() {
		return responsabile;
	}
	public void setResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCapienza() {
		return capienza;
	}
	public void setCapienza(int capienza) {
		this.capienza = capienza;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	public void setNumeroTelefono(String telefono) {
		this.numeroTelefono = telefono;
	}
	public void addAllievo(Allievo e) {
		this.allievi.add(e);
	}

	public void addAttivita(Attivita a) {
		this.attivita.add(a);
	}

}
