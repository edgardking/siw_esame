package it.edoardocostantini.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@Entity
public class Responsabile {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nome;
	private String cognome;
	@Column(nullable = false,unique=true)
	private String username; //email
	private String telefono;
	@Column(nullable = false)
	private String password;
	private String role;
	
	@OneToOne
	private Centro centro;
	

	
	public Responsabile() {
		this.role = "RESPONSABILE";
	}

	
	
	public String getRole() {
		return role;
	}





	public void setRole(String role) {
		this.role = role;
	}



	
	public Responsabile(String nome, String cognome, String username, String telefono, Centro centro) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.username = username;
		this.telefono = telefono;
		this.centro = centro;
		this.role="RESPONSABILE";
	}
	
	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		this.password = encoder.encode(password);
	}

	public Centro getCentro() {
		return centro;
	}
	public void setCentro(Centro centro) {
		this.centro = centro;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



}
