package it.edoardocostantini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.edoardocostantini.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo,Long> {

	List<Allievo> findByNome (String nome);
	List<Allievo> findByCognome (String cognome);
	List<Allievo> findByDataNascita (Date dataNascita);
	List<Allievo> findByEmail (String email);
	List<Allievo> findByResidenza (String residenza);
	
	List<Allievo> findByNomeAndCognome (String nome, String cognome);
	List<Allievo> findByNomeAndCognomeAndDataNascita (String nome, String cognome, Date dataNascita);
	List<Allievo> findByNomeAndCognomeAndEmail (String nome, String cognome, String email);
	List<Allievo> findByNomeAndCognomeAndDataNascitaAndEmail (String nome, String cognome, Date dataNascita, String email);
}
