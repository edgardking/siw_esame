package it.edoardocostantini.repository;

import java.util.*;

import org.springframework.data.repository.CrudRepository;

import it.edoardocostantini.model.Centro;

public interface CentroRepository extends CrudRepository<Centro,Long> {

	List<Centro> findByNome (String nome);
	List<Centro> findByCapienza (int capienza);
	List<Centro> findByIndirizzo (String indirizzo);
	List<Centro> findByEmail (String email);
	List<Centro> findByNomeAndCapienza (String nome, int capienza);
	List<Centro> findByNomeAndIndirizzo (String nome, String indirizzo);
	List<Centro> findByNomeAndCapienzaAndIndirizzo(String nome, int capienza, String indirizzo);
	Centro findByNumeroTelefono(String telefono);
	
}
