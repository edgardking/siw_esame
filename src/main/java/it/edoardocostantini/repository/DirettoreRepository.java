package it.edoardocostantini.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.edoardocostantini.model.Direttore;

public interface DirettoreRepository extends CrudRepository<Direttore,Long>{
	Direttore findByUsername(String username);
	Direttore findByPassword(String password);
	List<Direttore> findByNome(String nome);
	List<Direttore> findByCognome(String cognome);
	
}
