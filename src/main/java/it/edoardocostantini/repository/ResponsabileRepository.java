package it.edoardocostantini.repository;

import java.util.*;

import org.springframework.data.repository.CrudRepository;

import it.edoardocostantini.model.Responsabile;

public interface ResponsabileRepository extends CrudRepository<Responsabile,Long> {

	List<Responsabile> findByNome (String nome);
	List<Responsabile> findByCognome (String cognome);
	Responsabile findByUsername (String username);
	List<Responsabile> findByTelefono (String telefono);
	Responsabile findByPassword(String password);
	List<Responsabile> findByNomeAndCognome (String nome, String cognome);
	
}
