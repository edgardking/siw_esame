package it.edoardocostantini.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.edoardocostantini.model.Attivita;
import it.edoardocostantini.model.Centro;

public interface AttivitaRepository extends CrudRepository<Attivita,Long>{

	List<Attivita> findByGiorno(String day);
	List<Attivita> findByNome(String nome);
	List<Attivita> findByNomeAndGiorno(String nome, String day);
	List<Attivita> findByCentro(Centro centro);
 	
}
