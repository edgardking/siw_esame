package it.edoardocostantini.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.edoardocostantini.model.Attivita;


@Component
public class ResponsabileValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Attivita.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
	
		ValidationUtils.rejectIfEmpty(errors, "nome", "required");
		ValidationUtils.rejectIfEmpty(errors, "cognome", "required");
		ValidationUtils.rejectIfEmpty(errors, "username", "required");
		ValidationUtils.rejectIfEmpty(errors, "telefono", "required");

	}

}
