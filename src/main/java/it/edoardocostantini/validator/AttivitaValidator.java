package it.edoardocostantini.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.edoardocostantini.model.Attivita;

@Component
public class AttivitaValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Attivita.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "nome", "required");
		ValidationUtils.rejectIfEmpty(errors, "giorno", "required");
		ValidationUtils.rejectIfEmpty(errors, "ora", "required");

	}


}
