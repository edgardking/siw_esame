package it.edoardocostantini;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.edoardocostantini.model.Responsabile;
import it.edoardocostantini.service.ResponsabileService;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })

public class SpringTutorial0Application extends SpringBootServletInitializer {

		
	public static void main(String[] args) {
		SpringApplication.run(SpringTutorial0Application.class, args);
		System.out.println(new BCryptPasswordEncoder().encode("edoardo"));
		
	}

	
	@Autowired
	ResponsabileService respService;
	
	@PostConstruct
	public void init() {
		
	}
}
